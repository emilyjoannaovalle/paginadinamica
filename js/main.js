// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries


import { getDatabase ,onValue, ref as refS, set, child, get, update, remove } from
"https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";

//storage
import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyAaeONhsE5P6blrVeCw8SMWTQrl9MKfuUA",
    authDomain: "administradorweb-595b7.firebaseapp.com",
    databaseURL: "https://administradorweb-595b7-default-rtdb.firebaseio.com",
    projectId: "administradorweb-595b7",
    storageBucket: "administradorweb-595b7.appspot.com",
    messagingSenderId: "369664447002",
    appId: "1:369664447002:web:b7640db8f0089b1ced77a3"
  };

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

// Variables de la imagen
const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

//declarar unas Variables global
var CodigoProducto = "";
var tipoDeOro = "";
var Categoria = "";
var descripcion = "";
var urlImag = "";

  // listar productos
  Listarproductos(CodigoProducto)
  function Listarproductos() {
    const titulo= document.getElementById("titulo");
    const  section = document.getElementById('secArticulos')
    titulo.innerHTML= CodigoProducto; 
    
    
  const dbRef = refS(db, 'Productos');
  const tabla = document.getElementById('tablaProductos');
  
  onValue(dbRef, (snapshot) => {
      snapshot.forEach((childSnapshot) => {
          const childKey = childSnapshot.key;
  
          const data = childSnapshot.val();
         
  
        
  
          section.innerHTML+= "<div class ='card'> " +
                     "<img  id='urlImag' src=' "+ data.urlImag + "'  alt=''  width='200'> "
                     + "<h1 id='CodigoProducto'  class='title'>" + data.CodigoProducto+ ":" + data.Categoria +"</h1> "
                     + "<p  id='descripcion' class ='anta-regurar'>" + data.descripcion 
                     +  "</p> <button> Mas Informacion </button> </div>"  ;           
  
  
      });
  }, { onlyOnce: true });
  }
  
  